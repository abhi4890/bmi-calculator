export default class BMIView {
  constructor(viewRoot, model) {
    this.viewRoot = viewRoot;
    this.model = model;

    this.configureEvents();
  }

  configureEvents() {
    this.viewRoot.find(".btn-compute").on("click", () => {
      this.viewRoot.find("#result").text(this.model.calculate());
    });

    this.viewRoot.find("#weight").on("change",() => {
      this.model.setWeight(this.viewRoot.find("#weight").val());
    });

    this.viewRoot.find("#height").change(() => {
      this.model.setHeight(this.viewRoot.find("#height").val());
    });

  }
}
