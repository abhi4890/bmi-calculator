export class BMI {
  constructor(weight, height) {
    this.weight = weight;
    this.height = height;
  }

  calculate() {
    return this.weight / (this.height * this.height);
  }

  setWeight(weight) {
    this.weight = weight;
  }

  setHeight(height) {
    this.height = height;
  }

  getWeight() {
    return this.weight;
  }

  getHeight() {
    return this.height;
  }
}
