import BMIView from "./BMIView";
import { BMI } from "./BMI";
import $ from "jquery";

$(document).ready(() => {
  new BMIView($(".a-bmi-view"), new BMI());
});
