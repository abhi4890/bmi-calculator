import { BMI } from "../../src/js/BMI";

describe("#calculate", () => {
  it("expects to return zero if weight is zero", () => {
    expect(new BMI(0, 10).calculate()).toEqual(0);
  });

  it("expects to return bmi if height and weight are not zero", () => {
    expect(new BMI(10, 10).calculate()).toBeCloseTo(0.10, 2);
  });
});
