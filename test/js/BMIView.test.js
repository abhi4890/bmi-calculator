import $ from "jquery";
import { BMI } from "../../src/js/BMI";
import BMIView from "../../src/js/BMIView";

describe("BMI View", () => {
  it("triggers computation on the model when compute button is clicked", () => {
    document.body.innerHTML =
      "<div id='a-bmi-view'>" +
      "<button class='btn-compute'></button></div>";
    let model = new BMI();
    model.calculate = jest.fn();
    let bmiView = new BMIView($("#a-bmi-view"), model);
    $(".btn-compute").click();
    expect(model.calculate).toHaveBeenCalled();
  });

  it("expects the result when compute button is clicked", () => {
    document.body.innerHTML =
      "<div id='a-bmi-view'>" +
      "<button class='btn-compute'></button>" +
      "<span id='result'></span></div>";

    let model = new BMI();
    model.calculate = jest.fn().mockReturnValue(2);
    let bmiView = new BMIView($("#a-bmi-view"), model);
    $(".btn-compute").click();
    expect($("#result").text()).toBe("2");
  });

  it("expects the change in weight according to value in input tag", () => {
    document.body.innerHTML =
      "<div id='a-bmi-view'>" +
      "<input id ='weight'/></div>"
    let model = new BMI();
    let bmiView = new BMIView($("#a-bmi-view"), model);
    $("#weight").val("78").change();
    expect(model.getWeight()).toBe("78");
  });

  it("expects the change in height according to value in input tag", () => {
    document.body.innerHTML =
      "<div id='a-bmi-view'>" +
      "<input id ='height'/></div>"
    let model = new BMI();
    let bmiView = new BMIView($("#a-bmi-view"), model);
    $("#height").val("1.7").change();
    expect(model.getHeight()).toBe("1.7");
  });
});
